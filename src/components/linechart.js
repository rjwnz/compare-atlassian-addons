import React, { Component } from 'react';
import * as d3 from "d3";

const X_PADDING = 55;
const Y_PADDING = 50;
const HEIGHT = 600;
const MAX_WIDTH = 1000;
const SIZE = 6;

const getScaledAmount = (data) => {
    if(!data.percentage) return data.amount;
    return data.percentage * data.amount / 100;
}

export default class LineChart extends Component {

    render(){
        //We are ignoring the unlimited users tier here - assuming it will be very similar to 10,000 users...
        let apps = this.props.apps;
        const width = this.props.width ? Math.min(MAX_WIDTH,this.props.width) : MAX_WIDTH;
        const svg = d3.select(this.node);
        // We want the counts to be a sorted list of possible counts. This is kind of awkward because of the way we have decided to structure the data
        let counts = Array.from(new Set(apps.map(app=>app.data.map(d=> d.unitCount)).flat(1))).sort((a,b)=>a-b);
        let amounts = apps.map(app=>app.data.map(d=> getScaledAmount(d))).flat(1);

        let xAxisScale = d3.scalePoint()
            .domain(counts)
            .range([X_PADDING, width - X_PADDING]);

        let xAxis = d3.axisBottom()
                        .scale(xAxisScale)
                        .tickValues(counts)
                        .tickFormat(d3.format(",.0f"));

        let yAxisScale = d3.scaleLinear()
                        .domain([0,d3.max(amounts, amount => parseInt(amount))])
                        .range([HEIGHT-Y_PADDING,Y_PADDING]);

        let yAxis = d3.axisLeft()
                        .scale(yAxisScale)
                        .ticks()
                        .tickFormat(d3.format(",.0f"));

        let lineFuntion = d3.line()
                        .x(function(d) { return xAxisScale(d.unitCount); })
                        .y(function(d) {
                            return yAxisScale(getScaledAmount(d));
                        })


        let getXscale = (x) => xAxisScale(x);
        let getYscale = (y) => yAxisScale(y); 

        return <svg ref={node => this.node = node}
                    width="100%" 
                    viewBox={`0 0 ${width} ${HEIGHT}`}
            
                    >
                    
                    <g className="xAxis" ref={ node =>
                        d3.select(node)
                        .call(xAxis)
                        .selectAll("text")
                        .attr("y", 0)
                        .attr("x", 25)
                        .attr("dy", ".35em")
                        .attr("transform", "rotate(90)")} 
                        transform={`translate(0,${HEIGHT-Y_PADDING})`}/>

                    <g className="yAxis" ref={ node => 
                        d3.select(node)
                        .call(yAxis)} 
                        transform={`translate(${X_PADDING},0)`}/>

                    {apps.filter(app => app.key !== "custom-app" || this.props.showCustom)
                    .map(app => {
                        let path = [<path 
                                key={app.key}
                                stroke={app.colour}
                                strokeWidth='2'
                                fill='none'
                                d={lineFuntion(app.data)}>
                                transform={`translate(${X_PADDING},0)`}
                            </path>]
                        let circles = app.data.map(d => {
                            return (<circle
                                className='datapoint'
                                key={d.editionId ? d.editionId : (app.name + d.unitCount)}
                                cx={getXscale(d.unitCount)}
                                cy={getYscale(getScaledAmount(d))} 
                                r={SIZE}
                                fill={app.colour}
                            >
                                <title><p>{d.description} Price: US${d.amount}</p></title>
                            </circle>)}).flat(1);
                        
                        
                        return path.concat(circles);
                    })}
                    
        
                </svg>
    }
}