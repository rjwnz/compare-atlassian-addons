import React, { Component } from 'react';

const style = {
    borderCollapse:'initial',
    marginLeft:'6%',
};

const sizes = [10,25,50,100,250,500,2000,10000];

export default class CustomPrices extends Component {

    onChange = (event) =>{
        let target = event.target;
        this.props.changePrices(target.getAttribute("data"), target.value);
    }

    render(){
        return (
        <table style={style}>
            <tbody>
            {sizes.map(
                (size, index)=>{
                return (
                <tr> 
                <td>{size} Users </td> 
                <td>
                    <input
                        data={index}
                        type="number"
                        placeholder={this.props.prices[index]}
                        onChange={this.onChange}
                    />
                </td>
                </tr>
                )}
            )}

            </tbody>
        </table>
        );
    }

}