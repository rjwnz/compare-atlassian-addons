import React, { Component } from 'react';
import ProductControl from './product-control';

const getMetrics = (distribution) => {
    let downloads = distribution.downloads;
    let installs = distribution.totalInstalls;
    let users = distribution.totalUsers;

    return (
        <div style={{marginRight:'30px', marginLeft:'auto', textAlign:'right'}}><p>Avg Size: {(users/installs).toFixed(0)}</p><p> Conversion Rate: {(installs/downloads).toFixed(2) * 100}%</p><p> Users: {users}</p> </div>
    )



}

export default class Legend extends Component {

    render(){
        let apps = this.props.apps;
        return ( 
            <div>
                {apps.map((app) => {
                    return(
                        <div key={app.name} style={{padding:'10px', verticalAlign:'middle', display:'flex'}}>
                            <svg height="10" width="20">
                                <rect key={app.name} x="2" y="0" width="10" height="10" rx="3" ry="3" fill={app.colour}/>
                            </svg> 
                            {app.name} 
                            {app.distribution ? getMetrics(app.distribution) : null}
                            {app.isProduct ? <ProductControl app={app} saveValue={this.props.saveValue}/> : null}
                        </div>
                    )})}
            </div>
        );
    }

}