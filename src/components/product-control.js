import React, { Component } from 'react';

export default class ProductControl extends Component {

    onChange = (event) => {
        this.props.saveValue(event.target.value, this.props.app.name);
    }

    render() {
        return (
        <div style={{ marginLeft:'auto'}}>
            <input
                type="number"
                placeholder='10'
                max={100}
                onChange={this.onChange}
          />
        </div>
        );
    }

}