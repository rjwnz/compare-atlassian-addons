import React, { Component } from 'react';
import Select, { AsyncSelect} from '@atlaskit/select';
import { Checkbox } from '@atlaskit/checkbox';
import CustomPrices from './customPrices';

const PLUGIN = "plugin";
const APP = "app";

const customStyles = {
  container(styles) {
    return { 
      ...styles,
       padding: '5px', 
      };
  },
};

export default class Controls extends Component {

    getOptionLabel = (option) => {
      return option.name;
    }

    getProductLabel = (product) => {
      return product.label;
    }

    onChange = (object, action, type) => {
      if(action.action === "select-option") 
        this.props.select(object[object.length -1], type);
      if(action.action === "remove-value" || action.action === "pop-value") 
        this.props.remove(action.removedValue, type);
      if(action.action === "clear")
        this.props.clear(type);
    }

    onChangeAddon = (object,action) => {
      this.onChange(object,action, PLUGIN);
    }

    onChangeApp = (object, action) => {
        this.onChange(object,action, APP);
    }

    toggleCustom = (object, action) => {
      this.props.toggleCustom();
    }

    toggleDataCenter = () =>{
        var select = this.refs.addons.select;
        this.props.toggleDataCenter()
          .then(()=> this.props.loadOptions())
          .then((options)=>{
              select.setState({
                defaultInputValue:"",
                defaultOptions:options,
              })
          });
    }

    render(){
        return (
          <div style={{float:'center'}}>
            <Checkbox
              label="Show custom price curve"
              onChange = {this.toggleCustom}
            /> 
            {this.props.showCustom && <CustomPrices changePrices={this.props.changePrices} prices={this.props.prices}/>}
            <Checkbox
              value="Datacenter"
              label="Only show datacenter compatible apps"
              name="Datacenter"
              onChange={this.toggleDataCenter} />
            <AsyncSelect
              ref = 'addons'
              loadOptions={this.props.loadOptions}
              getOptionLabel={this.getOptionLabel}
              getOptionValue={this.getOptionLabel}
              onChange = {this.onChangeAddon}
              isMulti
              placeholder="Add a plugin"
              styles={customStyles}
              defaultOptions
            />
            <Select
              className="compact-select"
              classNamePrefix="react-select"
              spacing="compact"
              options={this.props.products}
              placeholder="Add a base application"
              filterOptions={false}
              isMulti={true}
              styles={customStyles}
              onChange = {this.onChangeApp}
              />

            
              
          </div>
        )
    }

}