import React, { Component } from 'react';
import axios from 'axios';
import * as d3 from 'd3';
import LineChart from './components/linechart';
import Controls from './components/controls';
import '@atlaskit/css-reset';
import Legend from './components/legend';
import data from './data/base-pricing.csv';
const BASE_URL = "https://marketplace.atlassian.com";
const SEARCH_URL = "/rest/2/addons";

const colours = [
  "#4D4D4D",
  "#5DA5DA",
  "#FAA43A",
  "#60BD68",
  "#F17CB0",
  "#B2912F",
  "#B276B2",
  "#DECF3F",
  "#F15854"
]
const CUSTOM_KEY="custom-app"
const CUSTOM_DEFAULT = {
  name:"Custom Curve",
  colour:colours[0],
  key:CUSTOM_KEY,
  isProduct:false, 
  isDataCenter:false,
  data:[{unitCount:"10",amount:"1000"},{unitCount:"25",amount:"1000"},{unitCount:"50",amount:"1000"},{unitCount:"100",amount:"1000"},{unitCount:"250",amount:"1000"},{unitCount:"500",amount:"1000"},{unitCount:"2000",amount:"1000"} ,{unitCount:"10000",amount:"1000"}],
  distribution:null
}

class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      priceData:[CUSTOM_DEFAULT],
      index:1,
      datacenter:false,
      products:[],
    };
  }

  componentDidMount(){
    
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
    let products = [];
    d3.csv(data, function(product) {
      if(!product) return;
      
      let prices = Object.entries(product).map((entry) => {
          return {unitCount:entry[0],amount:entry[1]}
      }).filter(price => {
        return !isNaN(price.unitCount) && price.amount !== "";
      });
      products.push({
        label:product.Application,
        value:product.Application, 
        key:product.Application,
        isProduct:true,
        data:prices
      })
      
    }).then(()=>{
      this.setState({
        products:[...products],
      });
      window.products = products
    });
    window.addPrices = this.addPriceData;
    window.removeApp = this.removeApp;
  }
  
  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
    
  }

  updateWindowDimensions = ()=> {
    this.setState({ width: window.innerWidth ,});
  }

  getHosting(){
    return this.state.datacenter ? 'datacenter' : 'server';
  }

  toggleCustom = ()=>{
    this.setState({showCustom:!this.state.showCustom,})
  }

  getDataForAppId = (id, name) => {
    let hosting = this.getHosting();
    let priceUrl = `https://marketplace.atlassian.com/rest/2/addons/${id}/pricing/${hosting}/live/`
    let distributionUrl = `https://marketplace.atlassian.com/rest/2/addons/${id}/distribution/`

    let params = {
      "limit":"50"
    };
    axios.all([
      axios.get(priceUrl, { params }),
      axios.get(distributionUrl, {params})
    ]).then(
      axios.spread((priceResponse, distributionResponse)=> {
      //We're not yet looking at unlimited user tier
      let prices = priceResponse.data.items.slice(0,-1);
      let distribution = distributionResponse.data;
      
      this.addPriceData(prices, id, name + ` (${this.getHosting()})`, false, distribution);
      })).catch((error)=> console.error(error));
  }

  addPriceData = (prices, key, name, isProduct, distribution) => {
    console.log(distribution);
    const colour = colours[this.state.index % 9];
    let appData = {
      data: prices,
      colour: colour,
      key: key,
      isDataCenter: this.state.datacenter,
      name: name,
      isProduct:isProduct,
      distribution:distribution
    }

    this.setState({
      priceData: this.state.priceData.concat(appData),
      index:this.state.index+1
    })
  }

  addAppToChart = (app) => {
    if(app.isProduct) {
      app.data.forEach(data => data.percentage = 10);
      this.addPriceData(app.data, app.label, app.label, true);
    } else {
      this.getDataForAppId(app.key, app.name);
    } 
  }

  removeApp = (app, type) => {
    this.setState({
      priceData:this.state.priceData.filter((item)=>item.key !== app.key),
    })
  }

  clearAll = (type) => {
    if(type === "plugin"){
      this.setState({
        priceData:this.state.priceData.filter((item)=>item.isProduct),
      })
    } else {
      this.setState({
        priceData:this.state.priceData.filter((item)=>!item.isProduct),
      })
    }
  }

  modifyBasePercentage = (value, name) => {
    let product = this.state.priceData.find(data => data.name === name);
    product.data.forEach((d) => d.percentage = value);
    this.setState({
      priceData:[...this.state.priceData],
    })
  }

  changeCustomPrice = (index, value) => {
    let oldPrice = this.state.priceData.find(app => app.key === CUSTOM_KEY).data;
    console.log(oldPrice);
    let newPrice = [...oldPrice]
    newPrice[index].amount = value;

    let newPriceData = [...this.state.priceData];
    newPriceData[0].data = newPrice;

    this.setState({priceData: newPriceData,}); 
  }

  loadOptions = (input) => {
    return axios.get(BASE_URL + SEARCH_URL,{"params":{
      "text":input,
      "cost":"marketplace",
      "hosting":[this.getHosting()]
      }})
      .then(response => {
        return response.data._embedded.addons;
      })
      .catch(error => {
        console.error(error);
        return [];
      });
  }

  toggleDataCenter = async() => {
    await this.setState({
      datacenter:!this.state.datacenter,
    });
    return this.state.datacenter;
  }

  render() {
    return (
      <React.Fragment>
        <h2>Compare the price of Atlassian Apps</h2>
        <LineChart id="price-graph" apps={this.state.priceData} width={this.state.width} showCustom={this.state.showCustom}/>
        <Controls loadOptions={this.loadOptions} select={this.addAppToChart} toggleDataCenter={this.toggleDataCenter} 
                  remove={this.removeApp} products={this.state.products} clear={this.clearAll} 
                  showDataCenter={this.state.datacenter} toggleCustom={this.toggleCustom} 
                  showCustom={this.state.showCustom} changePrices={this.changeCustomPrice} 
                  prices={this.state.priceData[0].data.map(point => point.amount)}/>
        <Legend apps={this.state.priceData} saveValue={this.modifyBasePercentage} />
      </React.Fragment>
    );
  }
}

export default App;
