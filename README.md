# Compare Atlassian Third Party Addon / App Pricing

[Live Demo](https://tender-rosalind-274703.netlify.com/)

![Screenshot](screenshot.png)

Simple app using d3 and react to easily compare pricing for many different Atlassian add-ons. 

### Features
- Quick search with autocomplete
- View server and/or dataceneter pricing
- Compare many add-ons in the same visualisation
- Tooltips on datapoints

Uses [Atlassian's marketplace API](https://developer.atlassian.com/platform/marketplace/rest)

## Editing the project:

Start development server:
`npm run start`

Build Static site
`npm run build`
